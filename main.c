// main.c
//
// Hydroponics control program for TI TM4C123GXL, using Keil v5
// Controls various subsystems required to automate hydroponics operation
//
// This file is part of greenwall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-06-12

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "lladoware/clock.h"
#include "lladoware/interrupts.h"
#include "lladoware/PLL.h"
#include "lladoware/timers.h"
#include "control.h"

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void programs_init(void);
void programs_update(void);

////////////////////////////////////////////////////////////////////////////////
// Global Constants

#define PROGRAM_TICK_FREQUENCY    1U  // Hz
#define PROGRAM_TICK_PERIOD_SYS   SYS_FREQ/PROGRAM_TICK_FREQUENCY
#define PROGRAM_TICK_PERIOD_CLK   CLOCK_SECOND/PROGRAM_TICK_FREQUENCY
#define PROGRAM_TICK_PRIORITY     2U

////////////////////////////////////////////////////////////////////////////////
// int main()

int main(void) {
  // Initialize all hardware
  PLL_Init();
  programs_init();

  clock_init();
  // set clock to noon
  for (uint32_t i = 0U; i < 12U; i++) {
    clock_increment_hr();
  }

  Timer2_Init(PROGRAM_TICK_PERIOD_SYS, PROGRAM_TICK_PRIORITY, &programs_update);

  // All further actions performed by interrupt handlers
  while(1) {
    WaitForInterrupt();
  }
}

////////////////////////////////////////////////////////////////////////////////
// programs_init()
// Run initialization actions for control programs

void programs_init(void) {
  grow_lights_init();
  water_pumps_init();
}

////////////////////////////////////////////////////////////////////////////////
// programs_update()
// Run update actions for control programs

void programs_update(void) {
  const uint32_t time_now = clock_time();

  // Update grow lights
  if((time_now % GROW_LIGHTS_PERIOD - GROW_LIGHTS_OFFSET) < GROW_LIGHTS_DUTY) {
    grow_lights_on();
  } else {
    grow_lights_off();
  }

  // Update water pumps
  if((time_now % WATER_PUMP_PERIOD - WATER_PUMP_OFFSET) < (2U * CLOCK_SECOND)) {
    water_pumps_run();
  } else {
    water_pumps_off();
  }
}

////////////////////////////////////////////////////////////////////////////////
// End of file
