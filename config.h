// config.h
//
// Configuration parameters for a Travis' hydroponics system
//
// This file is part of greenwall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-06-11

////////////////////////////////////////////////////////////////////////////////
// Constants

#define GROW_LIGHTS_DUTY      (12U * CLOCK_HOUR)
#define GROW_LIGHTS_OFFSET    (8U * CLOCK_HOUR)
#define GROW_LIGHTS_PERIOD    (1U * CLOCK_DAY)
#define GROW_LIGHTS_PIN       PA7

#define WATER_PUMP_DUTY       (5U * CLOCK_MINUTE)
#define WATER_PUMP_OFFSET     (12U * CLOCK_HOUR)
#define WATER_PUMP_PERIOD     GROW_LIGHTS_PERIOD
#define WATER_PUMP_01_PIN     PB2
#define WATER_PUMP_02_PIN     PB3
#define WATER_PUMP_03_PIN     PB7
#define NUM_ACTIVE_PUMPS      3U
#define WATER_PUMP_04_PIN     PB6
#define WATER_PUMP_05_PIN     PA4
#define WATER_PUMP_06_PIN     PA3
#define WATER_PUMP_07_PIN     PA2
#define WATER_PUMP_08_PIN     PA6
#define WATER_PUMP_09_PIN     PA5
#define WATER_PUMP_10_PIN     PB4
#define WATER_PUMP_11_PIN     PB1
#define WATER_PUMP_12_PIN     PB0
#define WATER_PUMP_13_PIN     PB5

////////////////////////////////////////////////////////////////////////////////
// End of file
