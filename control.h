// control.h
//
// Load control for TI TM4C123GXL using Keil v5
// A simple library that controls loads per a timer schedule using GPIO pins on
// a TI TM4C123GXL microcontroller
//
// This file is part of greenwall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-06-12

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "lladoware/output_pins.h"
#include "config.h"

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

void grow_lights_init(void);
void grow_lights_off(void);
void grow_lights_on(void);

void water_pumps_init(void);
void water_pumps_off(void);
void water_pumps_run(void);

////////////////////////////////////////////////////////////////////////////////
// End of file
