// control.h
//
// Load control for TI TM4C123GXL using Keil v5
// A simple library that controls loads per a timer schedule using GPIO pins on
// a TI TM4C123GXL microcontroller
//
// This file is part of greenwall v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-06-12

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "lladoware/clock.h"
#include "control.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

const uint32_t water_pump_pin[13U] = {
  WATER_PUMP_01_PIN, WATER_PUMP_02_PIN, WATER_PUMP_03_PIN, WATER_PUMP_04_PIN,
  WATER_PUMP_05_PIN, WATER_PUMP_06_PIN, WATER_PUMP_07_PIN, WATER_PUMP_08_PIN,
  WATER_PUMP_09_PIN, WATER_PUMP_10_PIN, WATER_PUMP_11_PIN, WATER_PUMP_12_PIN,
  WATER_PUMP_13_PIN};

////////////////////////////////////////////////////////////////////////////////
// grow_lights_init()
// initializes pin required for grow light control

void grow_lights_init(void) {
  init_pin(GROW_LIGHTS_PIN, 0U);
}

////////////////////////////////////////////////////////////////////////////////
// grow_lights_on()
// run grow lights on predetermined schedule

void grow_lights_on(void) {
  update_pin(GROW_LIGHTS_PIN, PIN_STATE_HIGH);
}

////////////////////////////////////////////////////////////////////////////////
// grow_lights_off()
// run grow lights on predetermined schedule

void grow_lights_off(void) {
  update_pin(GROW_LIGHTS_PIN, PIN_STATE_LOW);
}

////////////////////////////////////////////////////////////////////////////////
// water_pumps_init()
// Initializes pins required for water pump control

void water_pumps_init(void) {
  for(uint32_t i = 0U; i < NUM_ACTIVE_PUMPS; i++) {
    init_pin(water_pump_pin[i], 0U);
  }
  water_pumps_off();
}

////////////////////////////////////////////////////////////////////////////////
// water_pumps_off()
// Set all water pumps off

void water_pumps_off(void) {
  for(uint32_t i = 0U; i < NUM_ACTIVE_PUMPS; i++) {
    update_pin(water_pump_pin[i], PIN_STATE_HIGH);
  }
}

////////////////////////////////////////////////////////////////////////////////
// water_pumps_run()
// Run all water pumps in series

void water_pumps_run(void) {
    uint32_t time_now = 0U;

  void DisableInterrupts(void);
    for(uint32_t i = 0U; i < NUM_ACTIVE_PUMPS; i++) {
      time_now = clock_time();
      update_pin(water_pump_pin[i], PIN_STATE_LOW);
      while((clock_time() - time_now) < WATER_PUMP_DUTY) {}
      update_pin(water_pump_pin[i], PIN_STATE_HIGH);
    }
  void EnableInterrupts(void);
}

////////////////////////////////////////////////////////////////////////////////
// End of file
