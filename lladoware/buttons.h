// lladoware_buttons.h
//
// Onboard button management for TI TM4C123GXL using Keil v5
// A simple library that initializes and handles onboard buttons
// on a TI TM4C123GXL microcontroller
//
// This file is part of lladoware v1.0
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-05

////////////////////////////////////////////////////////////////////////////////
// Constants

#define DEBOUNCE_LENGTH 250 // milliseconds

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

// buttons_disable()
// Disables built-in buttons
void buttons_disable(void);

// buttons_enable()
// Enables built-in buttons
void buttons_enable(void);

// button_left_init()
// Initializes left built-in button with user-provided task
uint32_t button_left_init(const uint32_t priority, void(*task)(void));

// button_right_init()
// Initializes right built-in button with user-provided task
uint32_t button_right_init(const uint32_t priority, void(*task)(void));

////////////////////////////////////////////////////////////////////////////////
// End of file
