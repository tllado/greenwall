// adc.c

#include "tm4c123gh6pm.h"
#include "interrupts.h"
#include "adc.h"

typedef volatile uint32_t* register_type;

#define NUM_ADC_CHANNELS 12

const register_type ADC_GPIO_DIR_REG[NUM_ADC_CHANNELS] = {
  &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R,
  &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R, &GPIO_PORTD_DIR_R,
  &GPIO_PORTE_DIR_R, &GPIO_PORTE_DIR_R, &GPIO_PORTB_DIR_R, &GPIO_PORTB_DIR_R
};

const register_type ADC_GPIO_AFSEL_REG[NUM_ADC_CHANNELS] = {
  &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R,
  &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R, &GPIO_PORTD_AFSEL_R,
  &GPIO_PORTE_AFSEL_R, &GPIO_PORTE_AFSEL_R, &GPIO_PORTB_AFSEL_R, &GPIO_PORTB_AFSEL_R
};

const register_type ADC_GPIO_DEN_REG[NUM_ADC_CHANNELS] = {
  &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R,
  &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R, &GPIO_PORTD_DEN_R,
  &GPIO_PORTE_DEN_R, &GPIO_PORTE_DEN_R, &GPIO_PORTB_DEN_R, &GPIO_PORTB_DEN_R
};

const register_type ADC_GPIO_AMSEL_REG[NUM_ADC_CHANNELS] = {
  &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R,
  &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R, &GPIO_PORTD_AMSEL_R,
  &GPIO_PORTE_AMSEL_R, &GPIO_PORTE_AMSEL_R, &GPIO_PORTB_AMSEL_R, &GPIO_PORTB_AMSEL_R
};

const uint32_t ADC_SYSCTL_RCGCGPIO[NUM_ADC_CHANNELS] = {
  SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4,
  SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3, SYSCTL_RCGCGPIO_R3,
  SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R4, SYSCTL_RCGCGPIO_R1, SYSCTL_RCGCGPIO_R1
};

const uint32_t CHANNEL_INDEX[NUM_ADC_CHANNELS] = {
  0x08, 0x04, 0x02, 0x01, 0x08, 0x04, 0x02, 0x01, 0x02, 0x01, 0x01, 0x02
};

////////////////////////////////////////////////////////////////////////////////
// adc_init()
// Initializes specified ADC channel

void adc_init(const uint32_t channel_num){
  volatile uint32_t delay;

  SYSCTL_RCGCGPIO_R |= ADC_SYSCTL_RCGCGPIO[channel_num];

  *ADC_GPIO_DIR_REG[channel_num]   &= CHANNEL_INDEX[channel_num];
  *ADC_GPIO_AFSEL_REG[channel_num] |= CHANNEL_INDEX[channel_num];
  *ADC_GPIO_DEN_REG[channel_num]   &= CHANNEL_INDEX[channel_num];
  *ADC_GPIO_AMSEL_REG[channel_num] |= CHANNEL_INDEX[channel_num];

  SYSCTL_RCGCADC_R |= SYSCTL_RCGCADC_R0;  // activate ADC0
  delay = SYSCTL_RCGCADC_R;               // wait for it to initialize
  delay = SYSCTL_RCGCADC_R;
  delay = SYSCTL_RCGCADC_R;
  delay = SYSCTL_RCGCADC_R;

  ADC0_PC_R     = ADC_PC_SR_125K;                     // configure for 125K samples/sec
  ADC0_SSPRI_R  = 0x3210;                             // set sequencer priorities
  ADC0_ACTSS_R  &= ~ADC_RIS_INR3;                     // disable sample sequencer 3
  ADC0_EMUX_R   &= ~ADC_EMUX_EM3_M;                   // clear SS3 trigger select field
  ADC0_EMUX_R   += ADC_EMUX_EM3_PROCESSOR;            // configure for software trigger
  ADC0_SSCTL3_R = (ADC_SSCTL3_IE0 | ADC_SSCTL3_END0); // set flag and end
  ADC0_IM_R     &= ~ADC_IM_MASK3;                     // disable SS3 interrupts
  ADC0_ACTSS_R  |= ADC_RIS_INR3;                      // enable sample sequencer 3
}

////////////////////////////////////////////////////////////////////////////////
// adc_read()
// Reads current value from specified ADC channel

uint32_t adc_read(const uint32_t channel_num){
  uint32_t value;

  ADC0_SSMUX3_R = channel_num;  // Switch ADC to read desired channel

  ADC0_PSSI_R = 0x08; //initiate ADC sequence 3
  while((ADC0_RIS_R & 0x08) == 0) {}  //wait
  value = ADC0_SSFIFO3_R & 0xFFF; // 12-bit result
  ADC0_ISC_R = 0x08;// acknowledge ADC sequence 3 completion
  return value;
}

////////////////////////////////////////////////////////////////////////////////
// End of file
