// lladoware_timers.c
//
// Timers for TI TM4C123GXL using Keil v5
// Receive setup parameters and pointer to a task to be run
//
// This file is part of lladoware v1.1
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-04

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "tm4c123gh6pm.h"
#include "interrupts.h"
#include "timers.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

void(*Timer1ATask)(void);
void(*Timer2ATask)(void);

////////////////////////////////////////////////////////////////////////////////
// Timer1_Init()
// Initializes Timer1 to run user-specified task

uint32_t Timer1_Init(const uint32_t period, const uint32_t priority, void(*task)(void)) {
  const uint32_t failure = (priority > 7) ? 1 : 0;
  // if(!failure) {
    Timer1ATask = task;

    DisableInterrupts();
      SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R1;  // activate TIMER1
      while((SYSCTL_PRTIMER_R & SYSCTL_PRTIMER_R1) == 0) {} // wait for timer to start
      TIMER1_CTL_R = 0x00;                        // disable TIMER1A during setup
      TIMER1_CFG_R = TIMER_CFG_32_BIT_TIMER;      // set to 32-bit mode
      TIMER1_TAMR_R = TIMER_TAMR_TAMR_PERIOD;     // set to periodic mode
      TIMER1_TAILR_R = period - 1;                // set reset value
      TIMER1_TAPR_R = 0;                          // set bus clock resolution
      TIMER1_ICR_R = 0x01;                        // clear TIMER1A timeout flag
      TIMER1_IMR_R |= 0x01;                       // arm timeout interrupt
      NVIC_PRI5_R &= ~0x0000FF00;                 // clear previous priority
      NVIC_PRI5_R |= (priority << 13);            // set priority
      NVIC_EN0_R = NVIC_EN0_INT21;                // enable IRQ 21 in NVIC
      TIMER1_CTL_R = 0x01;                        // activate TIMER1A
    EnableInterrupts();
  // }

  return failure;
}

////////////////////////////////////////////////////////////////////////////////
// Timer1A_Handler()
// Time updater, called by timer interrupt

void Timer1A_Handler(void) {
  TIMER1_ICR_R = 0x01;  // acknowledge timeout
  Timer1ATask();
}

////////////////////////////////////////////////////////////////////////////////
// Timer2_Init()
// Initializes Timer2 to run user-specified task

uint32_t Timer2_Init(const uint32_t period, const uint32_t priority, void(*task)(void)) {
  const uint32_t failure = (priority > 7) ? 1 : 0;
  // if(!failure) {
    Timer2ATask = task;

    DisableInterrupts();
      SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R2;  // activate timer
      while((SYSCTL_PRTIMER_R & SYSCTL_PRTIMER_R2) == 0) {} // wait for timer to start
      TIMER2_CTL_R = 0x00;                        // disable TIMER2A during setup
      TIMER2_CFG_R = TIMER_CFG_32_BIT_TIMER;      // set to 32-bit mode
      TIMER2_TAMR_R = TIMER_TAMR_TAMR_PERIOD;     // set to periodic mode
      TIMER2_TAILR_R = period - 1;                // set reset value
      TIMER2_TAPR_R = 0;                          // set bus clock resolution
      TIMER2_ICR_R = 0x01;                        // clear timeout flag
      TIMER2_IMR_R |= 0x01;                       // arm timeout interrupt
      NVIC_PRI5_R &= ~0xFF000000;                 // clear previous priority
      NVIC_PRI5_R |= (priority << 29);            // set priority
      NVIC_EN0_R = NVIC_EN0_INT23;                // enable interrupt 23 in NVIC
      TIMER2_CTL_R = 0x01;                        // enable timer
    EnableInterrupts();
  // }

  return failure;
}

////////////////////////////////////////////////////////////////////////////////
// Timer2A_Handler()
// Runs provided task

void Timer2A_Handler(void) {
  TIMER2_ICR_R = 0x01;  // acknowledge timeout
  Timer2ATask();
}

////////////////////////////////////////////////////////////////////////////////
// End of file
