// lladoware_timing.c
//
// Timing functions for TI TM4C123GXL using Keil v5
// Clock that records time since startup using interrupts
//
// This file is part of lladoware v1.1
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-05

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "system_config.h"

////////////////////////////////////////////////////////////////////////////////
// Constants

#define CLOCK_MS      1
#define CLOCK_SECOND  (1000*CLOCK_MS)
#define CLOCK_MINUTE  (60*CLOCK_SECOND)
#define CLOCK_HOUR    (60*CLOCK_MINUTE)
#define CLOCK_DAY     (24*CLOCK_HOUR)

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

// clock_days()
// Returns number of days since startup
uint32_t clock_days(void);

// clock_hrs()
// Returns number of hours since end of last day
uint32_t clock_hrs(void);

// clock_increment_hr()
// Increments current time value to beginning of next hour
void clock_increment_hr(void);

// clock_increment_min()
// Increments current time value to beginning of next minute
void clock_increment_min(void);

// clock_init()
// Initializes clock that records time since startup
void clock_init(void);

// clock_initialized()
// Returns whether clock has been initialized
uint32_t clock_initialized(void);

// clock_mins()
// Returns number of minutes since end of last hour
uint32_t clock_mins(void);

// clock_secs()
// Returns number of seconds since end of last minute
uint32_t clock_secs(void);

// clock_time()
// Returns number of milliseconds since startup
uint32_t clock_time(void);

////////////////////////////////////////////////////////////////////////////////
// End of file
