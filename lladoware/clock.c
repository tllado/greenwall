// lladoware_timing.c
//
// Timing functions for TI TM4C123GXL using Keil v5
// Clock that records time since startup using interrupts
//
// This file is part of lladoware v1.1
// Travis Llado, travis@travisllado.com
// Last modified 2021-03-05

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "tm4c123gh6pm.h"
#include "clock.h"
#include "interrupts.h"
#include "timers.h"

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void clock_tick(void);

////////////////////////////////////////////////////////////////////////////////
// Constants

#define CLOCK_TICK_FREQUENCY  1000  // Hz
#define CLOCK_TICK_PRIORITY   0
#define CLOCK_TICK_PERIOD     (SYS_FREQ/CLOCK_TICK_FREQUENCY)

#define ROLLOVER_TIME 4233600000  // (uint32_t)~0 % CLOCK_DAY

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint32_t initialized = 0;
uint32_t time = 0;  // ms

////////////////////////////////////////////////////////////////////////////////
// clock_days()
// Returns number of days since startup

uint32_t clock_days(void) {
  return time / CLOCK_DAY;
}

////////////////////////////////////////////////////////////////////////////////
// clock_hrs()
// Returns number of hours since end of last day

uint32_t clock_hrs(void) {
  return (time % CLOCK_DAY) / CLOCK_HOUR;
}

////////////////////////////////////////////////////////////////////////////////
// clock_increment_hr()
// Increments current time value to beginning of next hour

void clock_increment_hr(void) {
  time += CLOCK_HOUR;
}

////////////////////////////////////////////////////////////////////////////////
// clock_increment_min()
// Increments current time value to beginning of next minute

void clock_increment_min(void) {
  time += (CLOCK_MINUTE - (clock_secs() * CLOCK_SECOND));
}

////////////////////////////////////////////////////////////////////////////////
// clock_init()
// Initializes clock that records time since startup

void clock_init(void) {
  initialized =
      !Timer1_Init(CLOCK_TICK_PERIOD, CLOCK_TICK_PRIORITY, &clock_tick);
}

////////////////////////////////////////////////////////////////////////////////
// clock_initialized()
// Returns whether clock has been initialized

uint32_t clock_initialized(void) {
  return initialized;
}

////////////////////////////////////////////////////////////////////////////////
// clock_mins()
// Returns number of minutes since end of last hour

uint32_t clock_mins(void) {
  return (time % CLOCK_HOUR) / CLOCK_MINUTE;
}

////////////////////////////////////////////////////////////////////////////////
// clock_secs()
// Returns number of seconds since end of last minute

uint32_t clock_secs(void) {
  return (time % CLOCK_MINUTE) / CLOCK_SECOND;
}

////////////////////////////////////////////////////////////////////////////////
// clock_tick()
// Increments clock by one millisecond

void clock_tick(void) {
  time++;

  // Rollover without jitter
  time %= ROLLOVER_TIME;
}

////////////////////////////////////////////////////////////////////////////////
// clock_time()
// Returns number of milliseconds since startup

uint32_t clock_time(void) {
  return time;
}

////////////////////////////////////////////////////////////////////////////////
// End of file
