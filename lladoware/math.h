// math.h

uint32_t mean(const uint32_t list[], const uint32_t length);

uint32_t median(uint32_t list[], const uint32_t length);

////////////////////////////////////////////////////////////////////////////////
// End of file
